class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get getName() {
    return this.name;
  }
  get getAge() {
    return this.age;
  }
  get getSalary() {
    return this.salary;
  }
  set getName(value) {
    this.name = value;
  }
  set getAge(value) {
    this.age = value;
  }
  set getSalary(value) {
    this.salary = value;
  }
}

class Programmer extends Employee {
  constructor(lang, name, age, salary) {
    super(name, age, salary);
    this.lang = lang;
  }

  getSalary() {
    return this.salary * 3;
  }
}

const human = new Employee();
const humanLang1 = new Programmer("English, Ukrain", "Dima", 21, 1200);
const humanLang2 = new Programmer("India, japan", "Vlad", 67, 300);
const humanLang3 = new Programmer("Russia, Moldova", "Sasha", 15, 800);
console.log(humanLang1);
console.log(humanLang2);
console.log(humanLang3);
