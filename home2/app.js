// Для того, что бы отловить исключение, используется конструкция try... catch. В блоке try выполняются операции, которые могут привести к исключительной ситуации, а блок catch позволяет 'словить' ошибку и ее обработать
const books = [
	{
		author: 'Скотт Бэккер',
		name: 'Тьма, что приходит прежде',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Воин-пророк',
	},
	{
		name: 'Тысячекратная мысль',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Нечестивый Консульт',
		price: 70,
	},
	{
		author: 'Дарья Донцова',
		name: 'Детектив на диете',
		price: 40,
	},
	{
		author: 'Дарья Донцова',
		name: 'Дед Снегур и Морозочка',
	},
];

const root = document.querySelector('#root');

function createList(arr, container) {
	for (let elem of arr) {
		const li = document.createElement('li');
		try {
			const requeredKeys = ['author', 'name', 'price'];

			for (let key of requeredKeys) {
				if (typeof elem[key] === 'undefined') {
					throw new Error(`${key}`);
				}
			}

			for (let key in elem) {
				const div = document.createElement('div');
				const p = document.createElement('p');
				p.innerHTML = `${key}: ${elem[key]}`;
				div.appendChild(p);
				li.appendChild(div);
			}
			container.appendChild(li);
		} catch (e) {
			console.log(`Error: ${e.message} is not defined`);
		}
	}
}

createList(books, root);
