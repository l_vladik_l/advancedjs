const btn = document.querySelector('button');
const container = document.querySelector('.container');

btn.addEventListener('click', loadInfo);

function loadInfo() {
	const DOMAIN = 'https://api.ipify.org/?format=json';

	getRequest();

	async function getRequest() {
		const ip = await getInf();
		const response = await fetch(`http://ip-api.com/json/${ip}`);
		const fullInfo = await response.json();
		createList(fullInfo);
	}

	async function getInf() {
		const response = await fetch(DOMAIN);
		const info = await response.json();
		let ip = info.ip;
		return ip;
	}

	function createList(fullInfo) {
		let infoIP = [
			fullInfo.as,
			fullInfo.country,
			fullInfo.region,
			fullInfo.city,
			fullInfo.regionName,
		];
		let infoText = ['Континент', 'Страна', 'Регион', 'Город', 'Район Города'];

		for (let i = 0; i < infoText.length; i++) {
			const p = document.createElement('p');

			p.textContent = `${infoText[i]} : ${infoIP[i]}`;
			container.appendChild(p);
		}
	}
}
