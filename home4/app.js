// AJAX это объединенная модель, которая включает в себя  HTML, CSS, JavaScript, DOM, XSLT, и объект XMLHttpRequest. С её помощью возможна реализация web-приложений способны делать быстрые обновления интерфейса пользователя без полной перезагрузки страницы браузером
const ul = document.querySelector('ul');
const DOMAIN = 'https://swapi.dev/api/films/';

function showInfo() {
	const response = fetch(DOMAIN);

	response
		.then((res) => res.json())
		.then((data) => {
			data.results.forEach((item) => {
				loadCharactersFilms(ul, item);
				loadEpisode(ul, item);
				loadDescriptions(ul, item);
			});
		});
}
showInfo();

function loadCharactersFilms(ul, item) {
	const li = document.createElement('li');
	li.innerHTML = item.title;
	ul.appendChild(li);
	item.characters.forEach((char) => {
		fetch(char)
			.then((res) => res.json())
			.then((data) => {
				const div = document.createElement('div');
				div.innerHTML = data.name;
				li.appendChild(div);
			});
	});
}

function loadEpisode(ul, item) {
	const episode = document.createElement('p');
	episode.innerHTML = `Episode: ${item.episode_id}`;
	ul.appendChild(episode);
}
function loadDescriptions(ul, item) {
	const descr = document.createElement('p');
	descr.innerHTML = `Description: ${item.opening_crawl}`;
	ul.appendChild(descr);
}

// fetch(DOMAIN)
// 	.then((res) => res.json())
// 	.then((data) => {
// 		data.results.forEach((i) => {
// 			let li = document.createElement('li');

// 			let ol = document.createElement('ol');

// 			li.innerHTML = i.title;

// 			li.appendChild(ol);

// 			ul.appendChild(li);

// 			let char = i.characters;

// 			char.forEach((name) => {
// 				fetch(name)
// 					.then((res) => res.json())
// 					.then((data) => {
// 						const liOl = document.createElement('li');

// 						liOl.innerHTML = data.name;

// 						ol.appendChild(liOl);
// 					});
// 			});
// 		});
// 	})
// 	.then(loadEpisode(ul, dat));

// function loadEpisode(ul, dat) {
// 	dat.forEach(({episode_id}) => {
// 		let id = document.createElement('p');
// 		id.innerHTML = episode_id;
// 		ul.appendChild(id);
// 	});
// }
